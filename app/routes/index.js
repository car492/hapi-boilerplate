const helloWorldRoutes = require('./hello-world.routes')

module.exports = [
  ...helloWorldRoutes
]
