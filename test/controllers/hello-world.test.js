/* eslint-env mocha */

const expect = require('chai').expect

const helloWorldController = require('../../app/controllers/hello-world.controller')

describe('Hello World Controller', function () {
  describe('index route', function () {
    it('should return "Hello World"', function () {
      const response = helloWorldController.index()

      expect(response).to.equals('Hello, world!')
    })
  })
})
